// SCROLL NAVBAR
$(function() {
    //$(".navbar-false").addClass("hidden");
    var header = $("#spectrum-navbar");

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {

            header.addClass("is-fixed-top");
            $(".navbar-false").removeClass("hidden");
        } else {
            header.removeClass("is-fixed-top");
            $(".navbar-false").addClass("hidden");
        } // if scroll
    });
}); // function

// OPEN MENU IN MOBILE
document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
});

// INFINITE SCROLL
var infScroll = new InfiniteScroll( '.article', {
  // defaults listed

  path: '.pagination-previous',
  append: '.columns',
  checkLastPage: '.pagination-previous',
  prefill: false,
  loadOnScroll: true,
  historyTitle: false,
  hideNav: '.pagination',
  status: '.scroller-status'

});

// SCROLL TO ELEMENT POST TEMPLATE
$("#scroll-to-content").click(function() {
    $('html, body').animate({
        scrollTop: $(".to-scroll-meta").offset().top
    }, 2000);
});

// SHOW/HIDE COMMENTS
$('.show-comments').click(function() {
  $('.open-comments').slideToggle('hidden');
  $('.fa', this).toggleClass('fa-plus fa-minus');
})

// DROPDOWN ACTIVE
$('.has-dropdown').click(function() {
  $('.navbar-dropdown').slideToggle('is-active');
})

// PROGRESS BAR
window.onscroll = function() {progressBar()};

function progressBar() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

// MODAL
$(".gs").click(function(){
  $("#" + $(this).data("target")).addClass('is-active').show( "slow" );
});

$(".dp").click(function(){
  $("#" + $(this).data("target")).addClass('is-active').show( "slow" );
});

$('.modal-close').click(function() {
  $('.modal').removeClass('is-active');
});

$('.modal-background').click(function() {
  $('.modal').removeClass('is-active');
});

// SHARE CONTENT WITH GNU SOCIAL AND DIASPORA
function gsShare() {

    var gs_node = document.getElementById('gs-node').value;
    var gs_msg = document.getElementById('gs-msg').value;
    var gs_url = document.getElementById('gs-url').placeholder;

    var httpProtocol = gs_node.substring(0,5);
    if (gs_node == "") {
        $('#gs-node').addClass('is-danger');
        $(gs_node).focus();
    } else {
        if (httpProtocol == 'https') {
            var urlNewWin = gs_node + '/notice/new?status_textarea=' + gs_msg + " " + gs_url;
            OpenInNewTab(urlNewWin);
        } else {
            var urlNewWin = 'https://' + gs_node + '/notice/new?status_textarea=' + gs_msg + " " + gs_url ;
            OpenInNewTab(urlNewWin);
        }
    }
}

function dpShare() {

    var dp_node = document.getElementById('dp-node').value;
    var dp_msg = document.getElementById('dp-msg').value;
    var dp_url = document.getElementById('dp-url').placeholder;

    var httpProtocol = dp_node.substring(0,5);
    if (dp_node == "") {
      $('#dp-node').addClass('is-danger');
      $(gs_node).focus();
    } else {
        if (httpProtocol == 'https') {
            var urlNewWin = dp_node + '/bookmarklet?url=' + dp_url + '&title=' + dp_msg + '&jump=doclose';
            OpenInNewTab(urlNewWin);
        } else {
            var urlNewWin = 'https://' + dp_node + '/bookmarklet?url=' + dp_url + '&title=' + dp_msg + '&jump=doclose';
            OpenInNewTab(urlNewWin);
        }
    }
}

function OpenInNewTab($url) {
    var win = window.open($url, '_blank');
    win.focus();
}

// CANVAS
(function() {

    var width, height, largeHeader, canvas, ctx, points, target, animateHeader = true;

    // Main
    initHeader();
    initAnimation();
    addListeners();

    function initHeader() {
        width = window.innerWidth;
        height = window.innerHeight;
        target = {x: width/2, y: height/2};

        largeHeader = document.getElementById('particle-header');
        largeHeader.style.height = height+'px';

        canvas = document.getElementById('particle-canvas');
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext('2d');

        // create points
        points = [];
        for(var x = 0; x < width; x = x + width/20) {
            for(var y = 0; y < height; y = y + height/20) {
                var px = x + Math.random()*width/20;
                var py = y + Math.random()*height/20;
                var p = {x: px, originX: px, y: py, originY: py };
                points.push(p);
            }
        }

        // for each point find the 5 closest points
        for(var i = 0; i < points.length; i++) {
            var closest = [];
            var p1 = points[i];
            for(var j = 0; j < points.length; j++) {
                var p2 = points[j]
                if(!(p1 == p2)) {
                    var placed = false;
                    for(var k = 0; k < 5; k++) {
                        if(!placed) {
                            if(closest[k] == undefined) {
                                closest[k] = p2;
                                placed = true;
                            }
                        }
                    }

                    for(var k = 0; k < 5; k++) {
                        if(!placed) {
                            if(getDistance(p1, p2) < getDistance(p1, closest[k])) {
                                closest[k] = p2;
                                placed = true;
                            }
                        }
                    }
                }
            }
            p1.closest = closest;
        }

        // assign a circle to each point
        for(var i in points) {
            var c = new Circle(points[i], 2+Math.random()*4, 'rgba(255,255,255,0.5)');
            points[i].circle = c;
        }
    }

    // Event handling
    function addListeners() {
        if(!('ontouchstart' in window)) {
            window.addEventListener('mousemove', mouseMove);
        }
        window.addEventListener('scroll', scrollCheck);
        window.addEventListener('resize', resize);
    }

    function mouseMove(e) {
        var posx = posy = 0;
        if (e.pageX || e.pageY) {
            posx = e.pageX;
            posy = e.pageY;
        }
        else if (e.clientX || e.clientY)    {
            posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        target.x = posx;
        target.y = posy;
    }

    function scrollCheck() {
        if(document.body.scrollTop > height) animateHeader = false;
        else animateHeader = true;
    }

    function resize() {
        width = window.innerWidth;
        height = window.innerHeight;
        largeHeader.style.height = height+'px';
        canvas.width = width;
        canvas.height = height;
    }

    // animation
    function initAnimation() {
        animate();
        for(var i in points) {
            shiftPoint(points[i]);
        }
    }

    function animate() {
        if(animateHeader) {
            ctx.clearRect(0,0,width,height);
            for(var i in points) {
                // detect points in range
                if(Math.abs(getDistance(target, points[i])) < 4000) {
                    points[i].active = 0.3;
                    points[i].circle.active = 0.6;
                } else if(Math.abs(getDistance(target, points[i])) < 20000) {
                    points[i].active = 0.1;
                    points[i].circle.active = 0.3;
                } else if(Math.abs(getDistance(target, points[i])) < 40000) {
                    points[i].active = 0.02;
                    points[i].circle.active = 0.1;
                } else {
                    points[i].active = 0;
                    points[i].circle.active = 0;
                }

                drawLines(points[i]);
                points[i].circle.draw();
            }
        }
        requestAnimationFrame(animate);
    }

    function shiftPoint(p) {
        TweenLite.to(p, 1+1*Math.random(), {x:p.originX-50+Math.random()*100,
            y: p.originY-50+Math.random()*100, ease:Circ.easeInOut,
            onComplete: function() {
                shiftPoint(p);
            }});
    }

    // Canvas manipulation
    function drawLines(p) {
        if(!p.active) return;
        for(var i in p.closest) {
            ctx.beginPath();
            ctx.moveTo(p.x, p.y);
            ctx.lineTo(p.closest[i].x, p.closest[i].y);
            ctx.strokeStyle = 'rgba(255,211,42,'+ p.active+')';
            ctx.stroke();
        }
    }

    function Circle(pos,rad,color) {
        var _this = this;

        // constructor
        (function() {
            _this.pos = pos || null;
            _this.radius = rad || null;
            _this.color = color || null;
        })();

        this.draw = function() {
            if(!_this.active) return;
            ctx.beginPath();
            ctx.arc(_this.pos.x, _this.pos.y, _this.radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = 'rgba(255,211,42,'+ _this.active+')';
            ctx.fill();
        };
    }

    // Util
    function getDistance(p1, p2) {
        return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
    }

})();
